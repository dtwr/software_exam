---
title: ""
layout: "about"
---

# 联系我

如果你碰到了软考过程中无法解决的问题，或者对本站有好的建议，欢迎随时联系我。

<img src="https://blog-1304855543.cos.ap-guangzhou.myqcloud.com/blog/image-20230105180220603.png" alt="image-20230105180220603" style="zoom: 33%;" />

你也可以通过我的博客更多的了解我：[个人站点](https://jycoder.club/)。

# 赞助我

维护不易，如果你觉得本站点帮助到了你，那就请我喝杯咖啡吧，那将是我持续更新的动力！

<img src="https://blog-1304855543.cos.ap-guangzhou.myqcloud.com/blog/image-20230106094125775.png" alt="image-20230106094125775" style="zoom: 25%;" />

# 更多帮助

作为一名老程序员，除辅导软考外，我也提供Java模拟面试、简历指导、面试材料、BUG修复、上线部署等等服务，你有任何问题，也可以随时联系我，告诉我你是通过这个网站来的，可以特别优惠哦。

## 鸣谢

本项目的绝大部分文档均来自于互联网，特别感谢:

- [软考之软件设计师资料](https://gitee.com/jianguo888/software-designer-test-paper)

- [zst_2001](https://space.bilibili.com/91286799)

- [软题库](https://www.ruantiku.com/course/4.html)

- [广东财经大学华商学院](http://www.ixinguan.com/pd.jsp?id=34)