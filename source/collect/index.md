---
title: ""
layout: "about"
---

# 刷题网站
- [小鸡软考](https://xkxxkx.cn/#/exam)

# 软件设计师

## 书籍材料

软件设计师教程第5版PDF：[点击下载](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABA9GAAgmZuBnQYohMfWrAc.pdf?f=%E8%BD%AF%E4%BB%B6%E8%AE%BE%E8%AE%A1%E5%B8%88%E6%95%99%E7%A8%8B%E7%AC%AC5%E7%89%88.pdf&v=1671450010)

软件设计师知识点分布DOCX：[点击下载](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABBBGAAgwMCBnQYokPKamAE.docx?f=%E8%BD%AF%E4%BB%B6%E8%AE%BE%E8%AE%A1%E5%B8%88%E7%9F%A5%E8%AF%86%E7%82%B9%E5%88%86%E5%B8%83.docx&v=1671454784)

资料合辑：[软件设计师2023](https://pan.baidu.com/s/1drqfdm86rR7ye5SHDiUrTg?pwd=y9hu)

## 在线视频

网易公开课在线视频：[软件设计师](https://open.163.com/newview/movie/free?pid=ZEUPRB16K&mid=JEUPRBA93)

## 历年真题

- 历年真题：[历年真题](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABBQGAAg2_D6nAYokZf8igY?f=%E5%8E%86%E5%B9%B4%E7%9C%9F%E9%A2%98%EF%BC%882009-2022%EF%BC%89.rar&v=1671344220)
- 真题解析：[真题解析](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABBQGAAg-d-6nAYolNWkjQI?f=%E7%9C%9F%E9%A2%98%E7%AD%94%E6%A1%88%EF%BC%882009-2022%EF%BC%89.rar&v=1671344126)
- 在线版：[软件设计师历年真题含答案与解析](https://ebook.qicoder.com/软件设计师/)

## 真题讲解

- [软件设计师2022年下半年上午真题解析](https://www.bilibili.com/video/BV1Fv4y1S7rU/?spm_id_from=333.999.0.0)

## 优质笔记

- 知乎笔记：https://zhuanlan.zhihu.com/p/394802434
- 软件设计师（上午题）：https://blog.csdn.net/qq_50954361/category_12089510
- 软件设计师（下午题）：https://blog.csdn.net/qq_50954361/category_12106267

# 系统分析师

## 在线视频

- 视频课程：[阿里云盘](https://www.aliyundrive.com/s/MdzHX5W2G8r)

# 系统架构师

## 在线视频

- 视频课程：[阿里云盘](https://www.aliyundrive.com/s/UVCNVEg2Pgd)