---
title: 软件设计师
date: 2023-01-03
author: 吉永超
---

# 软件设计师考试的主要内容

软件设计师，有时简称软设，软考考试的主要内容包括计算机组成原理、编译原理、操作系统、数据结构及算法、设计模式、软件工程、数据库等。

<!-- more -->

## 上午题知识点分布情况

| 教材内容                            | 知识点                                                       | 选择题题号   | 上午分值 | 下午分值                         |
| ----------------------------------- | ------------------------------------------------------------ | ------------ | -------- | -------------------------------- |
| 第1章 计算机系统知识                | 计算机系统基础知识、计算机体系结构 、安全性、可靠性与系统性能评测 | 1-6          | 6        | 0                                |
| 第2章  程序设计语言基础             | 程序设计语言的基本成分、语言处理程序基础、汇编程序基本原理、 编译程序基本原理、解释程序基本原理、python语言基础 | 20-22，50-52 | 6        | 0                                |
| 第3章 数据结构                      | 线性结构、数组、矩阵和广义表；树 、图 、查找、排序           | 59-65        | 7        | 15                               |
| 第4章 操作系统知识                  | 进程管理、存储管理、设备管理、文件管理、作业管理             | 23-28        | 6        | 0                                |
| 第5章 软件工程基础知识              | 软件过程模型、需求分析、系统设计、系统测试、运行和维护知识、软件项目管理、软件质量、软件度量 | 17-19，29-36 | 11       | 0                                |
| 第6章 结构化开发方法                | 结构化方法的基本思想、结构化分析方法、结构化设计方法、WebApp分析与设计、用户界面设计 | 15，16       | 2        | 15（概念结构设计、逻辑结构设计） |
| 第7章 面向对象技术                  | 面向对象方法的基本思想、UML、设计模式                        | 37-49        | 13       | 15（面向对象建模，用例图、类图） |
| 第8章 算法设计与分析                | 算法分析基础、分治法、动态规划法 、贪心法、回溯法、分支限界法 、概率算法、近似算法、数据挖掘算法、智能优化算法 | 66           | 1        | 0                                |
| 第9章 数据库技术基础                | 基本概念、数据模型、关系代数、关系数据库SQL语言简介、关系数据库的规范化、数据库的控制功能 | 53-58        | 6        | 15                               |
| 第10章 网络与信息安全基础知识       | 网络攻击、加解密技术、计算机病毒、安全协议、网络概述、网络互连硬件、网络的协议与标准 、Internet及应用 | 7-11，67-70  | 9        | 0                                |
| 第11章 标准化和软件知识产权基础知识 | 标准化基础知识，包括作品保护时间，侵权判定，知识产权归属，标准的分类，标准代号 | 12-14        | 3        | 0                                |

## 下午题知识点分布情况

共5题，前4题是必做题，第5题、第6题，两个题目选做一个。每题15分，共75分。

1. 试题一：数据流程图、数据字典
2. 试题二：数据库设计（概念结构设计、逻辑结构设计）
3. 试题三：面向对象分析与设计UML（用例图、类图为主）
4. 试题四：C语言程序填空、算法复杂度计算
5. 试题五：C++程序填空（二选一）
6. 试题六：Java程序填空（二选一）

# 软件设计师学习路线

由于每个人的学历背景、知识储备不尽相同，因此备考的时间长短也因人而异。大部分人都需要经过下面三个阶段：

1. 观看视频：此阶段大约需要占据整个备考时间的50%，如果你的基础不是很好，那建议你提前开始准备这部分内容
2. 刷真题：此阶段大约需要占据整个备考时间的35%，软考的题目上午和下午均有出现原题的可能性，且考点分布较为固定，拿到固定考点题目的分数就已经可以通过考试，因此这部分也需要认真对待
3. 模拟考试：此阶段大约需要占据整个备考时间的15%，这个阶段主要用来适应考试节奏和查缺补漏

对于大部分同学来说，我的建议是从报名那一天之后开始着手准备，对于基础较弱的同学，我建议在报名前一个月或者半个月开始准备。笔者在参加考试时，已经有三年的开发经验，大约准备了一个半月，最后顺利通过考试。

## 第一个阶段：观看视频

**特别提醒，视频中不需要观看的内容：**

- Flynn分类法
- 多媒体相关的内容

以上部分，在最近几年的考题中均未出现，可以选学，考试出现的概率很小。

<!-- 在线视频：[软件设计师](https://open.163.com/newview/movie/free?pid=ZEUPRB16K&mid=JEUPRB17S)。-->

<div style="width:1200px;height: 550px;margin:25px 0px 25px -150px;overflow: hidden;box-shadow:0px 0px 500px -74px rgba(0, 0, 0, 0.15);"> 
<div style="width: 1200px;height: 510px;margin:margin:25px 0px 25px -190px;overflow: hidden;"> 

  <iframe width="1400" height="650" frameborder="no" scrolling="no" name="wangyi" src="https://open.163.com/newview/movie/free?pid=ZEUPRB16K&amp;mid=JEUPRB17S" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="
    margin-top: -130px;
    margin-left: -100px;
">
</iframe>
</div>
</div>

这个阶段主要是要形成知识体系，系统的学习所有的知识点，虽然本站会提供[学习笔记](https://software-exam.pages.dev/notes/)，但还是强烈建议你在观看视频的同时，自己做一份笔记，方便日后总结回顾。

视频中的所有知识点都需要看一遍，软设考试的一个特点就是考试范围非常广，因此需要你每个知识点都了解，起码要有印象，刷真题的目的就是为了不断加深对这些知识的理解。

## 第二个阶段：刷真题

- 历年真题：[历年真题](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABBQGAAg2_D6nAYokZf8igY?f=历年真题（2009-2022）.rar&v=1671344220)
- 真题解析：[真题解析](http://download.s21i.faimallusr.com/8005329/0/0/ABUIABBQGAAg-d-6nAYolNWkjQI?f=真题答案（2009-2022）.rar&v=1671344126)

这个阶段主要是通过真题积累考试经验，了解考试的难度和题型，大致需要刷完最近十年的真题，建议从中间的年份开始刷，太早的有一些考点已经不会再考，太近的，这些题目最好留到第三阶段，用于模拟考试。

在刷题的同时，可以将发现的易错点，难点都记录到笔记里面，方便考前回顾。

## 第三个阶段：模拟考试

到了这个阶段就已经离成功很近了，这个时候需要的是提前适应好考试的节奏，因此需要进行两到三次的模拟考试。挑选最近两三年的真题，带好草稿纸，找到一个没有人打扰的地方，在规定的时间内，做完题目，然后对照答案批阅。

## 第四个阶段：查询成绩和领取证书

恭喜你成功上岸。

一般而言，考试成绩可以在考试后一个半月左右查询，三个月左右可以下载电子证书，五个月左右可以拿到纸质版证书。

## 移动端刷题软件

- 微信搜索小程序：软考刷题王
- APP：软考真题

## PC端刷题软件

你可以从下面我收集到的网站中选择一个你喜欢的：
- 软考真题：[软考真题](https://www.lightsoft.tech/)
- 在线电子书：[软件设计师历年真题含答案与解析](https://ebook.qicoder.com/软件设计师/)
- 希赛（部分收费）：[软件设计师历年真题库](https://www.educity.cn/rk/zhenti/prog/)

- 软题库：[链接](https://www.ruantiku.com/course/4.html)

# 历年真题解析

[zst_2001](https://space.bilibili.com/91286799)是我在备考的时候发现的宝藏UP主，他通过视频的方式，详细的介绍了每年考试真题的解析，相信对于备考的你而言，一定有所帮助。这里我按照年份分类，把链接贴出来，方便大家检索。

## 2020年-至今
- [软件设计师2023年上半年下午真题解析](https://www.bilibili.com/video/BV1hm4y1q7Cs/?spm_id_from=333.999.0.0)
- [软件设计师2023年上半年上午真题解析](https://www.bilibili.com/video/BV1WW4y197Am/?spm_id_from=333.999.0.0)
- [软件设计师2022年下半年上午真题解析](https://www.bilibili.com/video/BV1Fv4y1S7rU/?spm_id_from=333.999.0.0)
- [软件设计师2022年下半年下午真题解析](https://www.bilibili.com/video/BV1YG411w7wd/?spm_id_from=333.999.0.0)
- [软件设计师2022年上半年上午真题解析](https://www.bilibili.com/video/BV1rU4y1q7BD/?vd_source=d50ecb6af87b1c33b5246cc9b807d8fe)
- [软件设计师2022年上半年下午真题解析](https://www.bilibili.com/video/BV12a411n7W5/)
- [软件设计师2021年下半年上午真题解析](https://www.bilibili.com/video/BV1gU4y1C7QL/)
- [软件设计师2021年下半年下午真题解析](https://www.bilibili.com/video/BV1Vg411k7qq/)
- [软件设计师2021年上半年上午真题解析](https://www.bilibili.com/video/BV1CF411w7XN/)
- [软件设计师2021年上半年下午真题解析](https://www.bilibili.com/video/BV17Y4y1c73G/)
- [软件设计师2020年下半年上午真题解析](https://www.bilibili.com/video/BV1JT411A7rp/)
- [软件设计师2020年下半年下午真题解析](https://www.bilibili.com/video/BV1st4y1g7E8/)

## 2015年-2019年

- [软件设计师2019年下半年上午真题解析](https://www.bilibili.com/video/BV1kg411k7cq/)
- [软件设计师2019年下半年下午真题解析](https://www.bilibili.com/video/BV14g411k7Ah/)
- [软件设计师2019年上半年上午真题解析](https://www.bilibili.com/video/BV18F411w7ea/)
- [软件设计师2019年上半年下午真题解析](https://www.bilibili.com/video/BV1st4y1g7iV/)
- [软件设计师2018年下半年上午真题解析](https://www.bilibili.com/video/BV1mB4y1671k/)
- [软件设计师2018年下半年下午真题解析](https://www.bilibili.com/video/BV1Ug411k75L/)
- [软件设计师2018年上半年上午真题解析](https://www.bilibili.com/video/BV1xG4y1e7iA/)
- [软件设计师2018年上半年下午真题解析](https://www.bilibili.com/video/BV1St4y1g75a/)
- [软件设计师2017年下半年上午真题解析](https://www.bilibili.com/video/BV1Md4y1P71C/)
- [软件设计师2017年下半年下午真题解析](https://www.bilibili.com/video/BV1BB4y157pp/)
- [软件设计师2017年上半年上午真题解析](https://www.bilibili.com/video/BV1WV4y147DY/)
- [软件设计师2017年上半年下午真题解析](https://www.bilibili.com/video/BV1cv4y1c7mf/)
- [软件设计师2016年下半年上午真题解析](https://www.bilibili.com/video/BV1z14y1b7Xs/)
- [软件设计师2016年下半年下午真题解析](https://www.bilibili.com/video/BV1JV4y1479t/)
- [软件设计师2016年上半年上午真题解析](https://www.bilibili.com/video/BV1ad4y1K7fG/)
- [软件设计师2016年上半年下午真题解析](https://www.bilibili.com/video/BV1Ud4y1m7Y1/)
- [软件设计师2015年下半年上午真题解析](https://www.bilibili.com/video/BV1Zd4y1A7sp/)
- [软件设计师2015年下半年下午真题解析](https://www.bilibili.com/video/BV1bt4y1g7B1/)
- [软件设计师2015年上半年上午真题解析](https://www.bilibili.com/video/BV19N4y157dR/)
- [软件设计师2015年上半年下午真题解析](https://www.bilibili.com/video/BV16F411w7vi/)

## 2012年-2014年

- [软件设计师2014年下半年上午真题解析](https://www.bilibili.com/video/BV1XY4y1F7nn/?vd_source=d50ecb6af87b1c33b5246cc9b807d8fe)
- [软件设计师2014年下半年下午真题解析](https://www.bilibili.com/video/BV1pN4y1G7Tk/)
- [软件设计师2014年上半年上午真题解析](https://www.bilibili.com/video/BV1MT411A7i1/)
- [软件设计师2014年上半年下午真题解析](https://www.bilibili.com/video/BV15G411t7d1/)
- [软件设计师2013年下半年上午真题解析](https://www.bilibili.com/video/BV11B4y167tc/)
- [软件设计师2013年下半年下午真题解析](https://www.bilibili.com/video/BV1PY4y1c7wX/)
- [软件设计师2013年上半年上午真题解析](https://www.bilibili.com/video/BV1pF411w7SK/)
- [软件设计师2013年上半年下午真题解析](https://www.bilibili.com/video/BV13U4y1C7pD/?vd_source=d50ecb6af87b1c33b5246cc9b807d8fe)
- [软件设计师2012年下半年上午真题解析](https://www.bilibili.com/video/BV1nd4y1K7Z5/)
- [软件设计师2012年下半年下午真题解析](https://www.bilibili.com/video/BV1pt4y1g78a/)
- [软件设计师2012年上半年上午真题解析](https://www.bilibili.com/video/BV1Sa411o77M/?vd_source=d50ecb6af87b1c33b5246cc9b807d8fe)
- [软件设计师2012年上半年下午真题解析](https://www.bilibili.com/video/BV1KB4y1L7FA/)
