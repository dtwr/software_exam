---
title: ""
layout: "about"
hidden: true
---

# 学习心得

## 观其大略
这里借用诸葛亮“独观大略”的读书方法，在学习的过程中，不要在某一个知识点上死抠，而是把时间精力花在宏观知识体系的搭建上，那些当时怎么都没法理解的知识点，很有可能在你学完整个内容之后就豁然开朗了。

## 纸上得来终觉浅
学完知识点，一定要找一些题目练习一下，如果有编程的相关经验，下午的题目一定要在电脑上敲一敲。

## 唯有深入，才能浅出
软考的一个重要特点就是知识分布比较广，但是深度普通不高，遇到完全不懂的知识点，可以试试扩展一下和这个知识点相关的内容，深入的学习一下，这样才能做到深入浅出。

## 业精于勤，荒于嬉
通过考试，难的不是开始，而是坚持，希望看到这段文字的你，不要放弃，只要你坐在考场上，就已经有50%的概率可以通过了，加油。